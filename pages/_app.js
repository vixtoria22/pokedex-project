import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import { ApolloProvider } from "@apollo/client";
import { client } from "../src/services/gql";
import CustomGlobalStyles from "../src/components/CustomGlobalStyles";

function MyApp({ Component, pageProps }) {
  return (
    <ApolloProvider client={client}>
      <CustomGlobalStyles />
      <Component {...pageProps} />
    </ApolloProvider>
  );
}

export default MyApp;
