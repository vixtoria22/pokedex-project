import ArrowBackIosNew from "@mui/icons-material/ArrowBackIosNew";
import AppBar from "@mui/material/AppBar";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import Head from "next/head";
import { useRouter } from "next/router";
import { useState } from "react";
import CardPokemon from "../src/components/CardPokemon";
import FullLoading from "../src/components/FullLoading";
import TableBaseStats from "../src/components/TableBaseStats";
import usePokeApiDetail from "../src/hooks/usePokeApiDetail";
import BoxAbilities from "../src/components/BoxAbilities";
import ErrorBackdrop from "../src/components/ErrorBackdrop";

export default function Pokemon() {
  const router = useRouter();
  const { name } = router.query;
  const { data, loading, error } = usePokeApiDetail(name);
  const [tabActive, setTabActive] = useState(0);

  if (loading) return <FullLoading />;

  const pokemonData = data.species[0];
  const pokemonTypes = pokemonData.pokemons[0].types;

  if (error) return <ErrorBackdrop />;

  return (
    <div>
      <Head>
        <title>Pokedex | {pokemonData.name}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <AppBar component="nav">
          <Toolbar
            sx={{
              width: "95%",
              maxWidth: 576,
              mx: "auto",
            }}
          >
            <IconButton
              sx={{ color: "#fff" }}
              aria-label="filter"
              onClick={() => router.back()}
            >
              <ArrowBackIosNew />
            </IconButton>
            <Typography
              variant="h6"
              component="div"
              sx={{ flexGrow: 1, textTransform: "capitalize", marginLeft: 1 }}
            >
              {pokemonData.name}
            </Typography>
          </Toolbar>
        </AppBar>
        <Box
          sx={{
            maxWidth: "576px",
            margin: "0 auto",
            marginTop: 7,
            marginBottom: 15,
            justifyItems: "center",
          }}
        >
          <CardPokemon pokemonData={pokemonData} big />
          <Box sx={{ borderBottom: 1, borderColor: "divider", marginTop: 2 }}>
            <Tabs
              value={tabActive}
              onChange={(_, value) => {
                setTabActive(value);
              }}
              aria-label="basic tabs example"
            >
              <Tab label="Base Stats" />
              <Tab label="Abilities" />
            </Tabs>
          </Box>
          <TableBaseStats
            stats={pokemonData.pokemons[0].stats}
            hidden={tabActive !== 0}
            type={pokemonTypes[0].type.name}
            showProgress
            secondType={
              pokemonTypes.length > 1 ? pokemonTypes[1].type.name : ""
            }
          />
          <BoxAbilities
            hidden={tabActive !== 1}
            data={pokemonData.pokemons[0].abilities}
            type={pokemonTypes[0].type.name}
          />
        </Box>
      </main>
    </div>
  );
}
