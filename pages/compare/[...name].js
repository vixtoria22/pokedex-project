import ArrowBackIosNew from "@mui/icons-material/ArrowBackIosNew";
import AppBar from "@mui/material/AppBar";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Head from "next/head";
import { useRouter } from "next/router";
import CardPokemon from "../../src/components/CardPokemon";
import TableBaseStats from "../../src/components/TableBaseStats";
import usePokeApiCompare from "../../src/hooks/usePokeApiCompare";
import FullLoading from "../../src/components/FullLoading";
import ErrorBackdrop from "../../src/components/ErrorBackdrop";

export default function Pokemon() {
  const router = useRouter();
  const { name } = router.query;
  const { data, loading, error } = usePokeApiCompare(name);

  if (loading) return <FullLoading />;
  if (error) return <ErrorBackdrop />;

  const pokemonData = data.species;
  

  return (
    <div>
      <Head>
        <title>Pokedex | Compare Pokemons</title>
      </Head>

      <main>
        <AppBar component="nav">
          <Toolbar
            sx={{
              width: "95%",
              maxWidth: 576,
              mx: "auto",
            }}
          >
            <IconButton
              sx={{ color: "#fff" }}
              aria-label="filter"
              onClick={() => router.back()}
            >
              <ArrowBackIosNew />
            </IconButton>
            <Typography
              variant="h6"
              component="div"
              sx={{ flexGrow: 1, textTransform: "capitalize", marginLeft: 1 }}
            >
              Compare Pokemons
            </Typography>
          </Toolbar>
        </AppBar>
        <Box sx={{ maxWidth: 576, margin: "0 auto" }}>
          <Box
            sx={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gap: 1,
              marginTop: 10,
              marginBottom: 10,
              justifyItems: "center",
            }}
          >
            {pokemonData.map((thisPokemon) => (
              <Box
                sx={{ display: "flex", flexDirection: "column" }}
                key={thisPokemon.id}
                onClick={() => {
                  router.push({
                    pathname: "/[name]",
                    query: {
                      name: thisPokemon.name,
                    },
                  });
                }}
              >
                <CardPokemon pokemonData={thisPokemon} />
                <TableBaseStats
                  stats={thisPokemon.pokemons[0].stats}
                  type={thisPokemon.pokemons[0].types[0].type.name}
                  showProgress={false}
                  secondType={
                    thisPokemon.pokemons[0].types.length > 1
                      ? thisPokemon.pokemons[0].types[1].type.name
                      : ""
                  }
                />
              </Box>
            ))}
          </Box>
        </Box>
      </main>
    </div>
  );
}
