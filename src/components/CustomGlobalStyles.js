import GlobalStyles from "@mui/material/GlobalStyles";

export default function CustomGlobalStyles() {
  return (
    <GlobalStyles
      styles={{
        a: {
          color: "inherit",
          textDecoration: "none",
        },
        body: {
          margin: 0
        },
        ".normal": {
          "&&": {
            backgroundColor: "rgba(109, 109, 78, 0.1)",
            color: "rgb(109, 109, 78)",
          },
        },
        ".fire": {
          "&&": {
            backgroundColor: "rgba(233, 99, 3, 0.1)",
            color: "rgb(233, 99, 3)",
          },
        },
        ".water": {
          "&&": {
            backgroundColor: "rgba(79, 119, 190, 0.1)",
            color: "rgb(79, 119, 190)",
          },
        },
        ".electric": {
          "&&": {
            backgroundColor: "rgba(245, 209, 5, 0.1)",
            color: "rgb(212, 188, 52)",
          },
        },
        ".grass": {
          "&&": {
            backgroundColor: "rgba(115, 184, 97, 0.1)",
            color: "rgb(115, 184, 97)",
          },
        },
        ".ice": {
          "&&": {
            backgroundColor: "rgba(113, 186, 171, 0.1)",
            color: "rgb(113, 186, 171)",
          },
        },
        ".ground": {
          "&&": {
            backgroundColor: "rgba(206, 128, 86, 0.1)",
            color: "rgb(206, 128, 86)",
          },
        },
        ".flying": {
          "&&": {
            backgroundColor: "rgba(117, 140, 189, 0.1)",
            color: "rgb(117, 140, 189)",
          },
        },
        ".ghost": {
          "&&": {
            backgroundColor: "rgba(97, 110, 183, 0.1)",
            color: "rgb(97, 110, 183)",
          },
        },
        ".rock": {
          "&&": {
            backgroundColor: "rgba(132, 190, 179, 0.1)",
            color: "rgb(132, 190, 179)",
          },
        },
        ".fighting": {
          "&&": {
            backgroundColor: "rgba(196, 79, 99, 0.24)",
            color: "rgb(196, 77, 97)",
          },
        },
        ".poison": {
          "&&": {
            color: "rgb(172, 106, 202)",
            backgroundColor: "rgba(172, 106, 202, 0.1)",
          },
        },
        ".psychic": {
          "&&": {
            backgroundColor: "rgba(235, 139, 132, 0.1)",
            color: "rgb(235, 139, 133)",
          },
        },
        ".bug": {
          "&&": {
            backgroundColor: "rgba(155, 186, 72, 0.1)",
            color: "rgb(155, 186, 72)",
          },
        },
        ".dark": {
          "&&": {
            backgroundColor: "rgba(89, 87, 97, 0.1)",
            color: "rgb(89, 87, 97)",
          },
        },
        ".steel": {
          "&&": {
            backgroundColor: "rgba(101, 148, 161, 0.1)",
            color: "rgb(101, 148, 161)",
          },
        },
        ".dragon": {
          "&&": {
            backgroundColor: "rgba(44, 106, 193, 0.1)",
            color: "rgb(44, 106, 193)",
          },
        },
        ".fairy": {
          "&&": {
            backgroundColor: "rgba(226, 150, 225, 0.1)",
            color: "rgb(226, 150, 225)",
          },
        },
        ".active": {
          "&&": {
            backgroundColor: "#333",
            color: "#fff",
            fontWeight: 500,
          },
        },
      }}
    />
  );
}
