import Backdrop from "@mui/material/Backdrop";
import Typography from "@mui/material/Typography";

export default function ErrorBackdrop() {
  return (
    <div>
      <Backdrop sx={{ color: "#fff", zIndex: 10 }} open={true}>
        <Typography variant="h6" component="div">
          Error, please reload :(
        </Typography>
      </Backdrop>
    </div>
  );
}
