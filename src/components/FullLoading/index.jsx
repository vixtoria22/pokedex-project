import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";

export default function FullLoading() {
  return (
    <div>
      <Backdrop sx={{ color: "#fff", zIndex: 10 }} open={true}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}
