import Checkbox from "@mui/material/Checkbox";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Chip from "@mui/material/Chip";
import Typography from "@mui/material/Typography";
import { IMAGE_URL } from "../../utils/constant";
import Box from "@mui/material/Box";

export default function CardPokemon({
  pokemonData,
  big = false,
  showCheckbox = false,
  checked = false,
  onCheck,
}) {
  return (
    <Card
      sx={{
        cursor: big ? "" : "pointer",
        borderRadius: big ? "0 0 16px 16px" : 4,
      }}
    >
      {showCheckbox && (
        <Checkbox
          onClick={(e) => {
            e.preventDefault();
            onCheck();
          }}
          sx={{ position: "absolute" }}
          checked={checked}
        />
      )}
      <Box sx={{ background: "#eaeaea" }}>
        <CardMedia
          component="img"
          width={big ? 300 : 180}
          height={big ? 500 : 180}
          sx={{
            objectFit: "contain",
            display: "block",
            margin: "0 auto",
          }}
          alt={pokemonData.name}
          src={`${IMAGE_URL}${pokemonData.id}.png`}
        />
      </Box>
      <CardContent className={pokemonData.pokemons[0].types[0].type.name}>
        <Typography
          gutterBottom
          variant="h5"
          component="div"
          sx={{ textTransform: "capitalize" }}
        >
          {pokemonData.name}
        </Typography>
        {pokemonData.description && (
          <Typography
            variant="body2"
            color="text.secondary"
            sx={{
              height: {
                md: 75,
                xs: 125,
              },
            }}
          >
            {pokemonData.description[0].flavor_text}
          </Typography>
        )}
      </CardContent>
      <CardActions sx={{ background: "#eaeadd" }}>
        {pokemonData.pokemons[0].types.map((thisType) => (
          <Chip
            key={thisType.type.name}
            label={thisType.type.name}
            sx={{ border: 1 }}
            className={thisType.type.name}
          />
        ))}
      </CardActions>
    </Card>
  );
}
