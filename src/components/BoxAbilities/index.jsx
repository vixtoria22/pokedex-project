import Box from "@mui/material/Box";
import Chip from "@mui/material/Chip";

export default function BoxAbilities({ data, type, hidden }) {
  return (
    <Box sx={{ display: hidden ? "none" : "block" }} data-testid="box-abilities">
      {data.map((thisAbility) => (
        <Chip
          key={thisAbility.ability.name}
          label={thisAbility.ability.name}
          sx={{ border: 1, margin: 1 }}
          className={type}
        />
      ))}
    </Box>
  );
}
