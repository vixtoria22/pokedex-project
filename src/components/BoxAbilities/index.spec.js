import { fireEvent, render, screen } from "@testing-library/react";
import BoxAbilities from ".";

const dummyType = "dragon";

const dummyPokemonAbilities = [
  {
    ability: {
      name: "eating",
    },
  },
  {
    ability: {
      name: "sleeping",
    },
  },
];

describe("BoxAbilities", () => {
  test("should render dummy texts", () => {
    render(
      <BoxAbilities
        data={dummyPokemonAbilities}
        type={dummyType}
        hidden={false}
      />
    );
    const itemEating = screen.getByText("eating");
    const itemSleeping = screen.getByText("sleeping");
    expect(itemEating).toBeTruthy();
    expect(itemSleeping).toBeTruthy();
  });
  test("on hidden true, should not visible", () => {
    render(
      <BoxAbilities data={dummyPokemonAbilities} type={dummyType} hidden />
    );
    const boxAbilities = screen.getByTestId("box-abilities");
    expect(boxAbilities).not.toBeVisible();
  });
});
