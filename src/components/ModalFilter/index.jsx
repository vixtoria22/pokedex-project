import Button from "@mui/material/Button";
import Chip from "@mui/material/Chip";
import Modal from "@mui/material/Modal";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Box from "@mui/system/Box";
import DoneIcon from "@mui/icons-material/Done";

import React, { useState } from "react";
import * as ReactDOM from "react-dom";

export default function ModalFilter({ visible, onClose, data, updateFilter }) {
  const [selectedTypeFilter, setSelectedTypeFilter] = useState([]);
  const [selectedGenFilter, setSelectedGenFilter] = useState([]);

  const toggleSelectTypeFilter = (type) => {
    const foundId = selectedTypeFilter.find(
      (thisTypeid) => thisTypeid === type.types.id
    );
    if (foundId) {
      setSelectedTypeFilter([
        ...selectedTypeFilter.filter((thisTypeid) => thisTypeid !== foundId),
      ]);
      return;
    }
    setSelectedTypeFilter([...selectedTypeFilter, type.types.id]);
    return;
  };

  const toggleSelectGenFilter = (gen) => {
    const foundId = selectedGenFilter.find((thisGenId) => thisGenId === gen.id);
    if (foundId) {
      setSelectedGenFilter([
        ...selectedGenFilter.filter((thisGenId) => thisGenId !== foundId),
      ]);
      return;
    }
    setSelectedGenFilter([...selectedGenFilter, gen.id]);
    return;
  };

  const handleApplyFilter = () => {
    updateFilter({
      filterTypes: selectedTypeFilter,
      filterGens: selectedGenFilter,
      offset: 0,
    });
    onClose();
  };

  if (!visible) return null;

  return ReactDOM.createPortal(
    <React.Fragment>
      <Modal open={true} onClose={onClose}>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: 300,
            bgcolor: "#eaeaea",
            border: "21x solid #000",
            boxShadow: 24,
            borderRadius: 5,
            p: 4,
          }}
        >
          <Typography
            id="modal-modal-title"
            variant="h5"
            component="h5"
          >
            Filter
          </Typography>
          <Divider sx={{marginBottom: 2, marginTop: 2}} />
          <Box>
            <Typography variant="h6" component="h6">
              Types
            </Typography>
            <Box component="ul" sx={{ padding: 0 }}>
              {data.types.map((thisType) => {
                const active =
                  selectedTypeFilter.findIndex(
                    (thisSelectedTypeId) =>
                      thisSelectedTypeId === thisType.types.id
                  ) > -1;
                return (
                  <Button
                    component="li"
                    key={thisType.types.name}
                    onClick={() => toggleSelectTypeFilter(thisType)}
                  >
                    <Chip
                      className={`${thisType.types.name} ${
                        active ? "active" : ""
                      }`}
                      icon={active ? <DoneIcon /> : null}
                      sx={{ cursor: "pointer" }}
                      label={thisType.types.name}
                      variant="outlined"
                    />
                  </Button>
                );
              })}
            </Box>
          </Box>
          <Box>
            <Typography variant="h6" component="h6">
              Generation
            </Typography>
            <Box component="ul" sx={{ padding: 0 }}>
              {data.generations.map((thisGeneration) => {
                const active =
                  selectedGenFilter.findIndex(
                    (thisSelectedGenId) =>
                      thisSelectedGenId === thisGeneration.id
                  ) > -1;
                return (
                  <Button
                    component="li"
                    key={thisGeneration.name}
                    onClick={() => toggleSelectGenFilter(thisGeneration)}
                  >
                    <Chip
                      label={thisGeneration.name.replace("generation-", "")}
                      variant="outlined"
                      sx={{ cursor: "pointer" }}
                      icon={active ? <DoneIcon /> : null}
                      className={`${active ? "active" : ""}`}
                    />
                  </Button>
                );
              })}
            </Box>
          </Box>
          <Stack spacing={2} direction="row">
            <Button variant="contained" onClick={handleApplyFilter}>
              Apply Filter
            </Button>
            <Button variant="outlined" onClick={onClose}>
              Cancel
            </Button>
          </Stack>
        </Box>
      </Modal>
    </React.Fragment>,
    document.body
  );
}
