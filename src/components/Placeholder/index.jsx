import Skeleton from "@mui/material/Skeleton";

export default function Placeholder() {
  return [...Array(3)].map((_, index) => (
    <Skeleton
      key={`dummy_${index}`}
      variant="rectangular"
      width={180}
      height={250}
      sx={{ borderRadius: 4 }}
    />
  ));
}
