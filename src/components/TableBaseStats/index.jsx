import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Box from "@mui/material/Box";

export default function TableBaseStats({
  stats,
  hidden,
  type = "",
  secondType = "",
  showProgress = false,
}) {
  return (
    <TableContainer
      sx={{
        marginTop: 2,
        display: hidden ? "none" : "block",
      }}
      component={Paper}
    >
      <Table aria-label="caption table">
        <TableHead className={type}>
          <TableRow>
            <TableCell>Base Stats</TableCell>
            <TableCell align="right">Value</TableCell>
          </TableRow>
        </TableHead>
        <TableBody
          className={secondType}
          sx={{
            background: "#ddccbf",
          }}
        >
          {stats.map((thisStats) => (
            <TableRow key={thisStats.stat.name}>
              <TableCell
                component="th"
                scope="row"
                sx={{ textTransform: "capitalize", fontWeight: "500" }}
              >
                {thisStats.stat.name.replace("special-", "Sp.")}
              </TableCell>
              <TableCell align="right">
                <Box sx={{ display: "flex" }}>
                  {showProgress && (
                    <progress
                      value={thisStats.base_stat}
                      max="255"
                      style={{
                        marginRight: 5,
                        height: "1.5em",
                        flex: 1,
                        verticalAlign: "middle",
                      }}
                    />
                  )}
                  <span
                    style={{
                      display: "flex",
                      width: showProgress ? "15%" : "100%",
                      justifyContent: "flex-end",
                    }}
                  >
                    {thisStats.base_stat}
                  </span>
                </Box>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
