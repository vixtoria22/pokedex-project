import { useState } from "react";

export default function useModalFilter() {
  const [visible, setVisible] = useState(false);

  const toggle = () => {
    setVisible(!visible);
  };

  return { toggle, visible };
}
