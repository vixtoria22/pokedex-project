import { useQuery } from "@apollo/client";
import { GET_POKEMON_COMPARE } from "../services/gql";

export default function usePokeApiCompare(name) {
  const { data, loading, error } = useQuery(GET_POKEMON_COMPARE, {
    variables: { name },
  });

  return {
    error,
    data,
    loading,
  };
}
