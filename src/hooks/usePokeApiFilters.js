import { useQuery } from "@apollo/client";
import { GET_POKEMON_FILTERS } from "../services/gql";

export default function usePokeApiFilters() {
  const { data, loading } = useQuery(GET_POKEMON_FILTERS);

  return {
    data,
    loading,
  };
}
