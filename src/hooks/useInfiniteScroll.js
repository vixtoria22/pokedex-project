import { useState, useEffect } from "react";

export default function useInfiniteScroll() {
  const [triggerFetch, setTriggerFetch] = useState(false);
  const [hasNext, setHasNext] = useState(true);
  useEffect(() => {
    const handleScroll = () => {
      const currentHeight =
        window.innerHeight + document.documentElement.scrollTop;
      const minimumHeightToTrigger = document.body.offsetHeight * 0.9;


      if (currentHeight < minimumHeightToTrigger || !hasNext) return;
      setTriggerFetch(true);
    };
    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
  });

  return { triggerFetch, setTriggerFetch, setHasNext };
}
