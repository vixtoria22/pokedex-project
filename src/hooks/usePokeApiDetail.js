import { useQuery } from "@apollo/client";
import { GET_POKEMON_DETAIL } from "../services/gql";

export default function usePokeApiDetail(name) {
  const { data, loading, error } = useQuery(GET_POKEMON_DETAIL, {
    variables: { name },
  });

  return {
    error,
    data,
    loading,
  };
}
