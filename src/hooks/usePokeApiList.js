import { useQuery } from "@apollo/client";
import { useState } from "react";
import { GET_POKEMON_LIST } from "../services/gql";
import { DEFAULT_LIMIT } from "../utils/constant";

export default function usePokeApiList() {
  const [variables, setVariables] = useState({
    filterTypes: [],
    filterGens: [],
    offset: 0,
  });

  const { data, loading, error } = useQuery(
    GET_POKEMON_LIST(variables.filterTypes, variables.filterGens),
    {
      variables: {
        limit: DEFAULT_LIMIT,
        offset: variables.offset,
        filterGens: variables.filterGens,
        filterTypes: variables.filterTypes,
      },
    }
  );

  return {
    data,
    error,
    loading,
    variables,
    setVariables,
  };
}
