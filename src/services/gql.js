import { ApolloClient, gql, InMemoryCache } from "@apollo/client";

export const client = new ApolloClient({
  uri:
    process.env.NEXT_PUBLIC_GQL_URL || "https://beta.pokeapi.co/graphql/v1beta",
  cache: new InMemoryCache(),
});

export const GET_POKEMON_LIST = (filterTypes, filterGens) => {
  const gensQuery = filterGens.length
    ? `generation_id: {_in: $filterGens}`
    : "";
  const typesQuery = filterTypes.length
    ? `pokemon_v2_pokemons: {pokemon_v2_pokemontypes: {type_id: {_in: $filterTypes}}}`
    : "";

  const whereQuery = `
    ${gensQuery}
    ${typesQuery}
  `;

  return gql`
    query getPokemons(
      $limit: Int
      $offset: Int
      $filterGens: [Int!],
      $filterTypes: [Int!]
    ) {
      species: pokemon_v2_pokemonspecies(
        limit: $limit
        offset: $offset
        order_by: { id: asc }
        where: { ${whereQuery} }
      ) {
        id
        name
        pokemons: pokemon_v2_pokemons {
          id
          types: pokemon_v2_pokemontypes {
            type: pokemon_v2_type {
              name
            }
          }
        }
      }
      species_aggregate: pokemon_v2_pokemonspecies_aggregate(
        where: { ${whereQuery} }
      ) {
        aggregate {
          count
        }
      }
    }
  `;
};

export const GET_POKEMON_DETAIL = gql`
  query getPokemonDetail($name: String) {
    species: pokemon_v2_pokemonspecies(
      where: { name: { _eq: $name } }
      limit: 1
    ) {
      id
      name
      description: pokemon_v2_pokemonspeciesflavortexts(
        limit: 1
        where: { pokemon_v2_language: { name: { _eq: "en" } } }
      ) {
        flavor_text
      }
      egg_groups: pokemon_v2_pokemonegggroups {
        group: pokemon_v2_egggroup {
          name
        }
      }
      pokemons: pokemon_v2_pokemons(limit: 1) {
        id
        name
        height
        weight
        types: pokemon_v2_pokemontypes {
          type: pokemon_v2_type {
            name
          }
        }
        abilities: pokemon_v2_pokemonabilities {
          ability: pokemon_v2_ability {
            name
          }
        }
        stats: pokemon_v2_pokemonstats {
          base_stat
          stat: pokemon_v2_stat {
            name
          }
        }
      }
    }
  }
`;

export const GET_POKEMON_COMPARE = gql`
  query getPokemonCompare($name: [String!]) {
    species: pokemon_v2_pokemonspecies(where: { name: { _in: $name } }) {
      id
      name
      description: pokemon_v2_pokemonspeciesflavortexts(
        limit: 1
        where: { pokemon_v2_language: { name: { _eq: "en" } } }
      ) {
        flavor_text
      }
      pokemons: pokemon_v2_pokemons(limit: 1) {
        types: pokemon_v2_pokemontypes {
          type: pokemon_v2_type {
            name
          }
        }
        stats: pokemon_v2_pokemonstats {
          base_stat
          stat: pokemon_v2_stat {
            name
          }
        }
      }
    }
  }
`;

export const GET_POKEMON_FILTERS = gql`
  query getPokemonFilters {
    types: pokemon_v2_pokemontype(distinct_on: type_id) {
      types: pokemon_v2_type {
        name
        id
      }
    }
    generations: pokemon_v2_generation(distinct_on: id) {
      id
      name
    }
  }
`;
